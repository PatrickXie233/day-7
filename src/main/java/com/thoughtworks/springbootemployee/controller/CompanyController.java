package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.CompanyRepository;
import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/companies")
@RestController
public class CompanyController {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private CompanyRepository companyRepository;
    List<Company> companyList = new ArrayList<>();

    @GetMapping
    public List<Company> getCompanyList() {
        return companyRepository.getCompanyList();
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable int id) {
        return companyRepository.getCompanyById(id);
    }

    @PostMapping
    public int createCompany(@RequestBody Company company) {
        return companyRepository.createCompany(company);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable int id) {
        return employeeRepository.getEmployeeList().stream()
                .filter(employee -> employee.getCompanyId() == id)
                .collect(Collectors.toList());
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> getCompanyListByPage(int page, int size) {
        return companyRepository.getCompanyListByPage(page, size);
    }

    @PutMapping("/{id}")
    public Company updateCompany(@PathVariable int id, @RequestBody Company company) {
        return companyRepository.updateCompany(id, company);
    }

    @DeleteMapping("/{id}")
    public int deleteCompany(@PathVariable int id) {
        int deleteId = companyRepository.deleteCompany(id);
        employeeRepository.getEmployeeList().stream()
                .filter(employee1 -> employee1.getCompanyId() == deleteId)
                .collect(Collectors.toList())
                .forEach(employee -> {
                    employeeRepository.getEmployeeList().remove(employee);
                });
        return deleteId;
    }


    private Integer generateId() {
        int maxId = companyList.stream().mapToInt(Company::getId).max().orElse(0);
        return maxId + 1;
    }
}
